<?php
return [
    'your-username/your-repository' => [
        'secret' => 'a real secret',
        'branch' => [
            'main' => [
                'path' => [
                    '' => 'path/to/the/directory/to/be/synced',
                ]
                'basedir' => 'htdocs/',
                'use-git' => false,
                'script' => [
                    'after' => [
                        'php7.3 ../forums/bin/phpbbcli.php cache:purge  &> /dev/null &',
                    ]
                ]
            ]
        ]
    ],
];
