<?php declare(strict_types=1);

// ini_set('display_errors', 'On');
// error_reporting(E_ALL);

function main() {
    header("Content-Type: text/plain");

    $config = include('config.php');

    $data = file_get_contents('php://input');

    // cache the request during development
    // file_put_contents('data.txt', $data);
    // file_put_contents('server.json', json_encode($_SERVER));

    // $_SERVER = json_decode(file_get_contents('server.json'), true);
    // $data = file_get_contents('data.txt');
    // print('_SERVER:\n'.print_r($_SERVER, true)."\n");
    // print('data:\n'.print_r($data, true)."\n");

    $origin = explode('/', ($_SERVER['HTTP_USER_AGENT'] ?? []))[0] ?? '';

    // TODO: only pass the headers and name it headers?
    $values = Values::create($origin, $_SERVER, $data);

    if (is_null($values)) {
        http_response_code(403);
        die('request from an unknown origin: '. $origin);
    }

    $repository = $values->get_repository();
    // print('repository:\n'.print_r($repository, true)."\n");

    if (!array_key_exists($repository, $config)) {
        http_response_code(403);
        die('unsupported repository: '. $repository);
    }

    $config = $config[$repository];

    if (!$values->is_valid_secret($config['secret'])) {
        http_response_code(403);
        die('invalid secret token');
    }

    $source_event = $values->get_event();

    if (is_null($source_event)) {
        http_response_code(403);
        die('no action defined');
    }

    if ($source_event === 'ping') {
        die('got the ping');
    }

    if ($source_event !== $values->push_event) {
        http_response_code(403);
        die('invalid action: '. $source_event);
    }

    $branch = $values->get_branch();
    // print('branch:\n'.print_r($branch, true)."\n");

    if (!array_key_exists($branch, $config['branch'])) {
        http_response_code(403);
        die('unsupported branch: '. $branch);
    }

    if (is_null($config['branch'])) {
        die('ignored branch');
    }

    $config = $config['branch'][$branch];

    $path = $config['path'];

    foreach ($path as $key => &$value) {
        $value = rtrim($value, '/');
        if ($value === '' || !is_dir($value)) {
            http_response_code(403);
            die('invalid target path: '. $value);
        }
    }
    unset($value);

    if ($config['use-git'] ?? false) {
        // only one path is possible
        $key = array_key_first($path);
        shell_exec( 'cd '.$path[$key].' && git reset --hard HEAD && git pull' );
        shell_exec( 'cd '.$path[$key].' && git fetch --depth=1 && git reflog expire --expire-unreachable=now --all && git gc --aggressive --prune=all' );
        die('ok');
    }

    $queue = [];
    foreach ($values->get_commits() as $commit) {
        foreach (array_merge($commit['added'], $commit['modified']) as $item) {
            $queue[$item] = 'get';
        }
        foreach ($commit['removed'] as $item) {
            $queue[$item] = 'remove';
        }
    }

    print('queue:\n'.print_r($queue, true)."\n");

    $actions = [];

    foreach ($queue as $filename => $action) {
        if (!is_valid_filename($filename)) {
            continue;
        }

        $source_url = $values->get_source_url($repository, $branch, $filename);
        // ignore files that are outside of basedir
        list($target_path, $basedir_filename) = get_basedir_and_filename($config['path'] ?? null, $filename);
        // print('target_path:\n'.print_r($target_path, true)."\n");
        // print('basedir_filename:\n'.print_r($basedir_filename, true)."\n");
        // file_put_contents('log.txt', "filename: $filename\nbasedir_filename: $basedir_filename\n", FILE_APPEND);
        if (is_null($basedir_filename)) {
            continue;
        }

        $target_filename = $target_path.'/'.$basedir_filename;
        if ($action === 'remove') {
            if (get_http_response_code($source_url) !== 404) {
                echo("$filename not deleted\n");
                continue;
            }
            if (file_exists($target_filename)) {
                unlink($target_filename);
            }
        } elseif ($action === 'get') {
            if (get_http_response_code($source_url) !== 200) {
                echo("$filename from $source_url not updated\n");
                continue;
            }
            $content = file_get_contents($source_url);
            $parent_directory = dirname($target_filename);
            if (!is_dir($parent_directory) && !is_file($parent_directory)) {
                $umask = umask();
                mkdir($parent_directory, 0777, true);
            }
            file_put_contents($target_filename, $content);
            $actions[] = "get $target_filename from $source_url";
        }
    }

    // print('actions:\n'.print_r($actions, true)."\n");
    // file_put_contents('actions.json', json_encode($actions));

    /*
    if (array_key_exists('script', $config)
        && array_key_exists('after', $config['script'])
        ) {
        $script_after = $config['script']['after'];
        if (!is_array($script_after)) {
            echo('illformed list of scripts after\n');
        } else {
            foreach ($script_after as $script) {
                shell_exec($script);
            }
        }
    }
    */

    echo('ok');
}

main();

// The script is over
// Now some classes and functions used in the script.

// https://github.com/symfony/polyfill/blob/main/src/Php80/Php80.php
function str_starts_with($haystack, $needle) {
    return 0 === strncmp($haystack, $needle, strlen($needle));
}

// TODO: rename to something more... better.
abstract class Values {
    protected $server;
    protected $raw_data;
    protected $data;
    public $push_event = null;

    public static function create($origin, $server, $data) {
        if ($origin == 'GitLab') {
            return new GitLabValues($server, $data);
        } elseif ($origin == 'GitHub-Hookshot') {
            return new GitHubValues($server, $data);
        }
        return null;
    }

    public function __construct($server, $raw_data) {
        $this->server = $server;
        $this->raw_data = $raw_data;
        $this->data = json_decode($raw_data, true);
    }

    abstract public function get_event();
    abstract public function get_repository();
    abstract public function get_branch();
    abstract public function is_valid_secret($secret);
    abstract public function get_commits();
    abstract public function get_source_url($repository, $branch, $filename);
}

class GitLabValues extends Values {
    public $push_event = 'Push Hook';
    public function get_event() {
        return $this->server['HTTP_X_GITLAB_EVENT'] ?? null;
    }
    public function get_repository() {
        return ($this->data['project'] ?? [])['path_with_namespace'] ?? null;
    }
    public function get_branch() {
        return explode('/', $this->data['ref'] ?? '')[2] ?? null;
    }
    public function is_valid_secret($secret) {
        return $secret === ($server['HTTP_X_GITLAB_TOKEN'] ?? null);
    }
    public function get_commits() {
        return $this->data['commits'];
    }
    public function get_source_url($repository, $branch, $filename) {
        return "https://gitlab.com/$repository/-/raw/$branch/$filename?inline=false";
    }
}

class GitHubValues extends Values {
    public $push_event = 'push';
    public function get_event() {
        return $this->server['HTTP_X_GITHUB_EVENT'] ?? null;
    }
    public function get_repository() {
        return ($this->data['repository'] ?? [])['full_name'] ?? null;
    }
    public function get_branch() {
        return explode('/', $this->data['ref'] ?? '')[2] ?? null;
    }
    public function is_valid_secret($secret) {
        return 'sha1='.hash_hmac('sha1', $this->raw_data, $secret) === ($this->server['HTTP_X_HUB_SIGNATURE'] ?? null);
    }
    public function get_commits() {
        return $this->data['commits'];
    }
    public function get_source_url($repository, $branch, $filename) {
        return "https://raw.githubusercontent.com/$repository/$branch/$filename";
    }
}

// TODO: add the following three function as static in Values?

/**
 * a filename is not allowed to contain empty parts or pairs of dots.
 *
 * TODO: make sure that the filename does not contain other strange characters that might help to traverse the directory tree.
 */
function is_valid_filename($path) {
    $parts = explode('/', str_replace('\\', '/', $path));
    foreach ($parts as $part) {
        if ($part === '') {
            return false;
        } elseif ($part === '..') {
            return false;
        }
    }
    return true;
}

/**
 * returns 200 if the file exists, 404 if it does not
 */
function get_http_response_code($url) {
    $headers = get_headers($url);
    if ($headers === false) {
        return 0;
    }
    // echo('code '.substr($headers[0], 9, 3)."\n");
    return intval(substr($headers[0], 9, 3));
}

/**
 * if basedir is not null,
 * - if path starts with it, return the string with the basedir removed
 * - otherwise return null
 * otherwise return the string
 */
function get_basedir_and_filename($path, $filename) {
    if (is_null($path)) {
        return ['', $filename];
    }
    foreach ($path as $key => $value) {
        if (str_starts_with($filename, $key)) {
            return [$value, substr($filename, strlen($key))];
        }
    }
    return null;
}
