# git-sync

PHP script (and instructions) for using git to keep a web project in sync with a Gitlab or Github repository.

It can use an instance of Git installed on the server or (by default) pull the single files updated.


- It only manages files that are in the Git repository.
- Local changes on tracked files will be overwritten as soon as the file gets modified in the repository.

You can use one sync script for multiple projects and branches, each in their own directory.

For your safety, only use this script for actions that are non destructive and which could be triggered at anytime by anybody from anywhere in the internet.

## Installing

- Add this script to your website (as an example as `sync/index.php`).
- Create a `config.php` based on `config-demo.php`.

## Setup the webhook on Gitlab

In Settings > Webhooks, add a webhook targeting this PHP script:

- Url: path to this script (http://your-server.org/sync)
- Secret Token: define a secret for authentication; you will put the same value in the config.php file on the server.
- Check the checkbox to trigger Push events (set the branch if you have multiple ones).

Now, each time your repository gets pushed (or edited in the Web editor), the server will pull the files that have been modified (or remove the deleted ones).

## Setup the webhook on Github

In the settings, add a webhook targeting this PHP script.

- url: path to this script (`http://your-server.org/sync/`)  
  (the final `/` might matter).
- Select json as the content type.
- Define a secret (you will put the same in your `config.php` file.

Each time your repository gets pushed, a `git reset` it will be triggered on your server.

## `use-git`

With `use-git`, the script uses `git reset` and `git pull` to to force the _tracked_ files on your server to match the ones in the remote Git repository:

- It will only reset files that are tracked through Git.
- It won't change any other files.
- Local changes in the tracked files will be undone.

Git must be installed on the server.

## Todo

- Delete empty directories.

## Notes

If we ever need curl to get the content:

```php
function get_url_content($url) {
    $result = '';
    // echo('<pre>url: '.print_r($url, 1).'</pre>');
    // $this->log[] = '>> '.$url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, 'aoloe/deploy-git');
    // curl_setopt($ch, CURLOPT_HEADER, true);
    // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    $result = curl_exec($ch);
    $info = curl_getinfo($ch);
    // \Aoloe\debug('info', $info);
    if (($info['http_code'] == '404') || ($info['http_code'] == '403')) {
        $result = false;
    }
    curl_close($ch);
    // echo('<pre>result: '.print_r($result, 1).'</pre>');
    return $result;
}
```

If we ever need the Gitlab API:

```php
$project_id = $data['project_id'];
$source_url = "https://gitlab.com/api/v4/projects/$project_id/repository/files/$filename?ref=$branch";
$content = json_decode($content, true);
file_put_contents($target_filename, base64_decode($content['content']));
```

using the API pulls a json with some info and a base64 encoded content

## Resources

- <https://gist.github.com/jplitza/88d64ce351d38c2f4198>: A basic PHP webhook handler for Github, just verifying the signature and some variables before calling another command to do the actual work
- https://developer.github.com/v3/guides/managing-deploy-keys/
- https://gist.github.com/zhujunsan/a0becf82ade50ed06115
- https://www.justinsilver.com/technology/github-multiple-repository-ssh-deploy-keys/
- https://gist.github.com/jexchan/2351996
- https://stackoverflow.com/questions/4565700/specify-private-ssh-key-to-use-when-executing-shell-command-with-or-without-ruby
- remove the local history: <https://stackoverflow.com/a/37099824/5239250>
